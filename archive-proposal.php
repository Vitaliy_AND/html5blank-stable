<?php acf_form_head(); ?>
<?php get_header(); ?>

<section class="the-title">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Finance Proposals</h2>
      </div>
    </div>
  </div>
</section>

<?php
if ( is_user_logged_in() & current_user_can('administrator') ) : ?>



<section class="generic-sec">
  <div class="container">
    <div class="vehicle-info">
    <div class="row">
      <div class="col-12">
        <h2>Pending Proposals</h2>
<?php
$loop = new WP_Query(
    array(
        'post_type' => 'proposal', // This is the name of your post type - change this as required,
        'posts_per_page' => 50, // This is the amount of posts per page you want to show
                                  'orderby' => 'date',
                          'order' => 'DESC',
                                    'meta_key'    => 'application_sent',
  'meta_value'  => '0',
    )
);
while ( $loop->have_posts() ) : $loop->the_post();
// The content you want to loop goes in here:
?>
 
    <a href="<?php the_permalink(); ?>" class="vehicle-archive-wrap">
      <?php if(get_field('post_status') == "no"): ?>
      <span class="newspan">New</span>  

      <?php else: ?>

    <?php endif; ?>

    <div class="vehicle-archive-col-1">
     <span>CUSTOMER</span><h3><?php the_field('title'); ?> <?php the_field('surname'); ?>, <?php the_field('postcode'); ?></h3>
    </div>
    <div class="vehicle-archive-col-2">
     <span>VEHICLE</span><h3><?php the_field('manufacturer'); ?>, <?php the_field('model'); ?></h3>
    </div>
    <div class="vehicle-archive-col-3">
     <span>REG</span><h3><?php the_field('registration'); ?></h3>
    </div>
    <div class="vehicle-archive-col-4">
     <span>STATUS</span> 

  <?php if(get_field('electronic_siganture')) : ?>
    <li><span><i class="fa fa-check-circle" aria-hidden="true" style="color: green"></i></span> Finance Terms Agreed</li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Finance Terms Not Agreed</li>
    <?php endif; ?>

  <?php if(get_field('application_sent') == "1") : ?>
    <li><span><i class="fa fa-check-circle" aria-hidden="true" style="color: green"></i></span> Application Email<br><small>Sent for review</small></li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Application not sent for Review?</li>
    <?php endif; ?>

    </div>
  </a>

<?php endwhile;
wp_reset_postdata();
?>

      </div>
    </div>
  </div>
</div>
</section>




<section class="generic-sec">
  <div class="container">
    <div class="vehicle-info">
    <div class="row">
      <div class="col-12">
        <h2>Showroom Proposals</h2>
<?php
$loop = new WP_Query(
    array(
        'post_type' => 'proposal', // This is the name of your post type - change this as required,
        'posts_per_page' => 50, // This is the amount of posts per page you want to show
                                  'orderby' => 'date',
                          'order' => 'DESC',
          'meta_key'    => 'proposal_type',
  'meta_value'  => 'showroom',
    )
);
while ( $loop->have_posts() ) : $loop->the_post();
// The content you want to loop goes in here:
?>
 
    <a href="<?php the_permalink(); ?>" class="vehicle-archive-wrap">
      <?php if(get_field('post_status') == "no"): ?>
      <span>New</span>  

      <?php else: ?>

    <?php endif; ?>

    <div class="vehicle-archive-col-1">
     <span>CUSTOMER</span><h3><?php the_field('title'); ?> <?php the_field('surname'); ?>, <?php the_field('postcode'); ?></h3>
    </div>
    <div class="vehicle-archive-col-2">
     <span>VEHICLE</span><h3><?php the_field('manufacturer'); ?>, <?php the_field('model'); ?></h3>
    </div>
    <div class="vehicle-archive-col-3">
     <span>REG</span><h3><?php the_field('registration'); ?></h3>
    </div>
    <div class="vehicle-archive-col-4">
     <span>STATUS</span> 

  <?php if(get_field('electronic_siganture')) : ?>
    <li><span><i class="fa fa-check-circle" aria-hidden="true" style="color: green"></i></span> Finance Terms Agreed</li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Finance Terms Not Agreed</li>
    <?php endif; ?>

  <?php if(get_field('application_sent') == "1") : ?>
    <li><span><i class="fa fa-check-circle" aria-hidden="true" style="color: green"></i></span> Application Email<br><small>Sent for review</small></li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Application not sent for Review?</li>
    <?php endif; ?>

    </div>
  </a>

<?php endwhile;
wp_reset_postdata();
?>

      </div>
    </div>
  </div>
</div>
</section>


<section class="generic-sec">
  <div class="container">
    <div class="vehicle-info">
    <div class="row">
      <div class="col-12">
        <h2>Home Proposals</h2>
<?php
$loop = new WP_Query(
    array(
        'post_type' => 'proposal', // This is the name of your post type - change this as required,
        'posts_per_page' => 50, // This is the amount of posts per page you want to show
          'meta_key'    => 'proposal_type',
  'meta_value'  => 'home',
    )
);
while ( $loop->have_posts() ) : $loop->the_post();
// The content you want to loop goes in here:
?>
 
    <a href="<?php the_permalink(); ?>" class="vehicle-archive-wrap">
    <div class="vehicle-archive-col-1">
     <span>CUSTOMER</span><h3><?php the_field('title'); ?> <?php the_field('surname'); ?>, <?php the_field('postcode'); ?></h3>
    </div>
    <div class="vehicle-archive-col-2">
     <span>VEHICLE</span><h3><?php the_field('manufacturer'); ?>, <?php the_field('model'); ?></h3>
    </div>
    <div class="vehicle-archive-col-3">
     <span>REG</span><h3><?php the_field('registration'); ?></h3>
    </div>
    <div class="vehicle-archive-col-4">
     <span>STATUS</span> 

  <?php if(get_field('electronic_siganture')) : ?>
    <li><span><i class="fa fa-check-circle" aria-hidden="true" style="color: green"></i></span> Finance Terms Agreed</li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Finance Terms Not Agreed</li>
    <?php endif; ?>

  <?php if(get_field('application_sent') == "1") : ?>
    <li><span><i class="fa fa-check-circle" aria-hidden="true" style="color: green"></i></span> Application Email<br><small>Sent for review</small></li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Application not sent for Review?</li>
    <?php endif; ?>

    </div>
  </a>

<?php endwhile;
wp_reset_postdata();
?>

      </div>
    </div>
  </div>
</div>
</section>



<?php else : ?>

<section class="generic-sec">
  <div class="container">
    <div class="vehicle-info">
    <div class="row">
      <div class="col-12">
        <p>Sorry, you have to be logged in to view this info.</p>
        <?php wp_login_form(); ?>
      </div>
    </div>
  </div>
</div>
</section>



<?php endif; ?>


<?php get_footer(); ?>

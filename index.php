<?php get_header(); ?>

<section class="front-sec">
  <div class="container">
    <div class="vehicle-info">
    <div class="row">
      <div class="col-6 front-tile">
      	<a href="https://identity.codeweavers.net/platform/codeweavers/default/app/showroom/login#?customer_id=bd825091-0fde-4b8c-9df9-0670d6f3ac70" target="_blank"><img src="/new-proposal.png"><span>START A NEW PROPOSAL</span></a>
      </div>
      <div class="col-6 front-tile">
      	<a href="https://forwardassetfinanceproposals-co-uk.stackstaging.com/proposal/"><img src="/current-proposal.png"><span>VIEW CURRENT PROPOSALS</span></a>
      </div>
     </div>
    </div>
   </div>
  </section>

<?php get_footer(); ?>

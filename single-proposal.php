<?php acf_form_head(); ?>
<?php get_header(); ?>
            <?php while ( have_posts() ) : the_post(); ?>
              
<?php
// STARTS - wrapp your content with this conditional statement
if ( post_password_required() ) : ?>


<section class="the-title">
  <div class="container">
    <div class="row">
      <div class="col-12">
      <?php echo get_the_password_form(); ?>
      <p>Please note, your password for the propodal documnet is your 'Surname' follwed by your, i.e 'Smith'.</p>
      </div>
    </div>
  </div>
</section>

<?php else : ?>


<section class="the-title">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Hi <?php the_field('title'); ?> <?php the_field('surname'); ?>, we are one step away from arranging your finance. <br>Please simply complete the form below to start your application.</h2>
      </div>
    </div>
  </div>
</section>


<?php update_field('post_status', 'new'); ?>

<div id="accordion">
    
<div class="container">
  <div class="vehicle-info">
  <div class="row">
    <div class="col-sm">
      <h2>Your Details</h2>
    
<!-- Customer Info --->    
    
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         <i class="fa fa-user" aria-hidden="true"></i> Personal Details (please check these are correct)
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">



<div class="container">
  <div class="row">
    <div class="col-sm">
        <h2>Personal Details</h2>
          <div class="row">
            <div class="col-6">


                <?php
          acf_form(array(
              'post_id'   => $post_id,
              'post_title'    => false,
              'field_groups' => array(7),
              'submit_value'  => 'Update Details'
          ));
        ?>


  </div>
</div>

    </div>
 
    
  </div>
</div>



      </div>
    </div>
  </div>
  
<!-- Vehicle Info --->   

  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         <i class="fa fa-map-marker" aria-hidden="true"></i> Address Details (please check these are correct)
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">

          <div class="row">
            <div class="col-6">


                <?php
          acf_form(array(
              'post_id'   => $post_id,
              'post_title'    => false,
              'field_groups' => array(24),
              'submit_value'  => 'Update Details'
          ));
        ?>

  </div>
</div>

      </div>
    </div>
  </div>
  
  <!-- Proposal Info --->   
  
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <i class="fa fa-briefcase" aria-hidden="true"></i> Employment Details (please check these are correct)
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">


<div class="col-sm">
        <h2>Employment History</h2>



        <?php
        acf_form(array(
            'post_id'   => $post_id,
            'post_title'    => false,
            'field_groups' => array(70),
            'submit_value'  => 'Update Details'
        ));
        ?>

    </div>


      </div>
    </div>
  </div>
  
  
  
    <div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          <i class="fa fa-university" aria-hidden="true"></i> Bank Details (please check these are correct)
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">

                  <div class="row">
            <div class="col-6">



                <?php
          acf_form(array(
              'post_id'   => $post_id,
              'post_title'    => false,
              'field_groups' => array(34),
              'submit_value'  => 'Update Details'
          ));
        ?>


  </div>
</div>

      </div>
    </div>
  </div>
  
</div>

</div>

</div>

</div>
</div>


<section class="vehicle-slider">
  <div class="container">
        <div class="vehicle-info">
    <div class="row">
     <!-- <div class="col-6">
        <div class="flexslider">
        <ul class="slides">
          <li>
            <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/a3-black-0916.jpg?itok=K6NwxrjY" />
          </li>
          <li>
            <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/a3-black-0916.jpg?itok=K6NwxrjY" />
          </li>
          <li>
            <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/a3-black-0916.jpg?itok=K6NwxrjY" />
          </li>
          <li>
            <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/a3-black-0916.jpg?itok=K6NwxrjY" />
          </li>
        </ul>
      </div> -->
      </div>
      <div class="col-12">

            <h2>What we are financing</h2>


                <?php if ( is_user_logged_in() ) : ?>

                <?php
                  acf_form(array(
                      'post_id'   => $post_id,
                      'post_title'    => false,
                      'field_groups' => array(49),
                      'submit_value'  => 'Update Details'
                  ));
                ?>

                <?php else: ?>

                  <label class="vehicle-label">MANUFACTURER</label>
                  <p><?php the_field('manufacturer'); ?></p>

                  <label class="vehicle-label">Model</label>
                  <p><?php the_field('model'); ?></p>

                  <label class="vehicle-label">Mileage</label>
                  <p><?php the_field('mileage'); ?></p>

                  <?php if(get_field('registration')) : ?><label class="vehicle-label">Registration</label>
                  <p><?php the_field('registration'); ?></p>
                <?php endif; ?>

                  <label class="vehicle-label">Transmission</label>
                  <p><?php the_field('transmission'); ?></p>

                <?php endif; ?>



      </div>

    </div>
    </div> 
  </div>
</section>

<section class="finance-form">
  <div class="container">
    <div class="vehicle-info">
            <h2>Finance Terms</h2>

     
                       <div class="row">
            <div class="col-6">

                <?php if ( is_user_logged_in() ) : ?>

                <?php
          acf_form(array(
              'post_id'   => $post_id,
              'post_title'    => false,
              'field_groups' => array(41),
              'submit_value'  => 'Update Details'
          ));
        ?>

                <?php else: ?>

                  <label class="vehicle-label">Type of Finance Needed?</label>
                  <p><?php the_field('type_of_finance_needed'); ?></p>

                  <label class="vehicle-label">Vehicle Price</label>
                  <p>£<?php the_field('vehicle_price'); ?></p>

                  <label class="vehicle-label">Deposit</label>
                  <p>£<?php the_field('deposit'); ?></p>

                  <label class="vehicle-label">Part Exchange</label>
                  <p>£<?php the_field('part_exchange'); ?></p>

                  <label class="vehicle-label">Outstanding Finance</label>
                  <p>£<?php the_field('outstanding_finance'); ?></p>

                  <label class="vehicle-label">Total Deposit</label>
                  <p>£<?php the_field('total_deposit'); ?></p>

                  <label class="vehicle-label">Anticipated Annual Mileage</label>
                  <p><?php the_field('anticipated_annual_mileage'); ?></p>

                  <label class="vehicle-label">Ideal Period of Finance</label>
                  <p><?php the_field('ideal_period_of_finance'); ?></p>

                  <label class="vehicle-label">Mothly Installments That Suit Your Budget?</label>
                  <p>£<?php the_field('mothly_installments_that_suit_your_budget'); ?></p>

                  <label class="vehicle-label">Annual Gross Salary</label>
                  <p>£<?php the_field('annual_gross_salary'); ?></p>

                  <label class="vehicle-label">Replacement or Existing Loan</label>
                  <p><?php the_field('replacement_or_existing_loan'); ?></p>

                  <label class="vehicle-label">Total Credit</label>
                  <p>£<?php the_field('total_credit'); ?></p>
                  
                <?php endif; ?>



  </div>
</div>





  </div> 
</div>
</section>

<?php if(get_field('email_details', $curauth) == "1") : ?>
<section class="generic-sec" id="agree">
  <div class="container">
    <div class="vehicle-info">
    <div class="row">
      <div class="col-12">
        <h2>Send Agreement</h2>

<div id="agreesend">
        <?php
          acf_form(array(
              'post_id'   => $post_id,
              'post_title'    => false,
              'field_groups' => array(64),
              'fields' => array('email_sent_validation'),
              'submit_value'  => 'Send Agreement',

          ));
        ?>
</div>

          <?php echo do_shortcode('[contact-form-7 id="61" title="Contact form 1"]'); ?>

    <?php if(get_field('email_sent')) : ?>

      <p>This email was last sent on: <?php the_field('email_sent'); ?>

    <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</section>
<?php else: ?>
<section class="generic-sec">
  <div class="container">
    <div class="vehicle-info">
    <div class="row">
      <div class="col-12">

 


<div id="proposal-sig">
                <?php
          acf_form(array(
              'post_id'   => $post_id,
              'post_title'    => false,
              'field_groups' => array(55),
              'submit_value'  => 'Update Application'
          ));
        ?>

<div id="form-approved"><?php echo do_shortcode('[contact-form-7 id="61" title="Proposal to Forward"]'); ?></div>
<div id="form-notready"><?php echo do_shortcode('[contact-form-7 id="282" title="Proposal to Forward_copy"]'); ?></div>

</div>


</div>
</div>
</div>
</div>
</section>
<?php endif; ?>



<style type="text/css">
  
  .wpcf7-form-control-wrap.your-name > input  {
    display: none !important;
  }

  .wpcf7-form-control-wrap.your-email > input {
    display: none !important;
  }

  .wpcf7-form-control-wrap.your-subject > input  {
    display: none !important;
  }

  .wpcf7-form-control-wrap.your-message > input {
  border-radius: 30px;
    border: 1px solid #d3d2d2;
    font-size: 15px !important;
    padding: 16px !important;
  }
</style>




<div id="the-list-check">
  <h3>Application Checklist / Status</h3>
  <ul>
    
  <?php if(get_field('electronic_siganture')) : ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true" style="color: green"></i></span> Finance Terms Agreed?</li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Finance Terms Agreed?</li>
    <?php endif; ?>

  <?php if(get_field('application_sent') == "1") : ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true" style="color: green"></i></span> Application Email<br><small>Sent for review</small></li>
<?php else: ?>
    <li><span><i class="fa fa-times-circle" aria-hidden="true"></i></span> Application not sent for Review?</li>
    <?php endif; ?>

  </ul>
</div>





<?php endif; ?>


    <?php endwhile; ?>



<?php get_footer(); ?>
